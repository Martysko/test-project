Instructions (try to commit after each step)

1. Fork the repository
2. Create branch "solution" from master
3. Create commit replacing "MASTER" (line 12) vvvvvvvvvvvvvvvvvv with "TEST" to master
4. Create commit replacing "MASTER" with "SOLUTION"
4. Create a Dockerfile for flask.py (from ubuntu) (install 2 pip packages from install.txt)
5. Create a docker-compose.yml for flask.py and database
6. Run your solution and try to open '0.0.0.0:5000/add_sample?data=new' in the Browser
7. Resolve conflicts with master by rebase - Change output from "MASTER" to "TEST SOLUTION"
8. Merge your branch to master
9. Create Merge request to original repository

MASTER
